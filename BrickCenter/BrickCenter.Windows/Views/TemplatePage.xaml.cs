﻿using BrickCenter.Common;
using BrickCenter.Models;
using BrickCenter.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace BrickCenter.Views
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class TemplatePage : Page
    {

        private NavigationHelper navigationHelper;
        public TemplatePageViewModel _model { get; set; } = new TemplatePageViewModel();
        public TemplatePage()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
        }
        #region NavigationHelper registration

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
            await _model.Load(e.Parameter as Template);
            _model.Frame = Frame;
            DataContext = _model;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void Motor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as GridView).SelectedItem != null)
            {
                _model.SelectedMotor = (sender as GridView).SelectedItem as Motor;
            }
        }

        private void DropMotorOnTrigger(object sender, DragEventArgs e)
        {
            _model.DropMotorOnTrigger(sender, e);

        }

        private void Trigger_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as GridView).SelectedItem != null)
            {
                _model.SelectedTrigger = (sender as GridView).SelectedItem as Trigger;
            }
        }

        private void MotorDragStarting(object sender, DragItemsStartingEventArgs e)
        {
            _model.MotorDragStarting(e);

        }

        private void cbPadNumbers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (_model.SelectedUserController.ControllerType == EControllerType.XboxPad)
            //{
            //    uint index = uint.Parse((sender as ComboBox).SelectedItem.ToString());
            //    _model.XboxPadController = new XboxPadController(index - 1, 100);
            //    _model.XboxPadController.Connected += XboxPadController_Connected;
            //    _model.XboxPadController.Disconnected += XboxPadController_Disconnected;
            //    _model.XboxPadController.StateChanged += XboxPadController_StateChanged;
            //    tbPadConnected.Visibility = Windows.UI.Xaml.Visibility.Visible;
            //    tbPadConnected.Text = "Disconnected";

            //}
            //else
            //    tbPadConnected.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }
    }
}
