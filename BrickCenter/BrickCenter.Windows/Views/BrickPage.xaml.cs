﻿using BrickCenter.Common;
using BrickCenter.Models;
using BrickCenter.Models.Enumerates;
using BrickCenter.ViewModels;
using PropertyChanged;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace BrickCenter.Views
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    [ImplementPropertyChanged]
    public sealed partial class BrickPage : Page
    {

        private NavigationHelper navigationHelper;
        public BrickPageViewModel _model { get; set; } = new BrickPageViewModel();
       
        public BrickPage()
        {

            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            Windows.UI.Xaml.Window.Current.CoreWindow.KeyDown += (sender, arg) =>
            {
                _model.KeyboardKeyPressed(arg);// invoked anytime a key is pressed down, independent of focus
            };
            Windows.UI.Xaml.Window.Current.CoreWindow.KeyUp += (sender, arg) =>
            {
                _model.KeyboardKeyReleased(arg);// invoked anytime a key is pressed down, independent of focus
            };
        }



        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
            var args = e.Parameter as BrickController;
            _model.BrickController = args;
            _model.BrickController.isConneted = true;
            _model.BrickController.Brick.BrickChanged += Brick_BrickChanged;
            await _model.Load();
            _model.Frame = Frame;
            DataContext = _model;

        }

        private void Brick_BrickChanged(object sender, Lego.Ev3.Core.BrickChangedEventArgs e)
        {
            foreach (var motor in _model.Motors)
            {
                motor.Update(_model.BrickController.Brick);
            }
            foreach (var sensor in _model.Sensors)
            {
                sensor.Update(_model.BrickController.Brick);
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        private void MotorDragStarting(object sender, DragItemsStartingEventArgs e)
        {
            _model.MotorDragStarting(e);
        }

        private void DropMotorOnTrigger(object sender, Windows.UI.Xaml.DragEventArgs e)
        {
            _model.DropMotorOnTrigger(sender,e);
        }

        private void UserController_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //_model.SelectedUserController = (sender as ComboBox).SelectedItem as UserController;
            //    cbPadNumbers.Visibility = _model.SelectedUserController.ControllerType == UserControllerType.XboxPad ?
            //        Windows.UI.Xaml.Visibility.Visible : Windows.UI.Xaml.Visibility.Collapsed;
        }

        private void Trigger_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if((sender as GridView).SelectedItem != null)
            {
                _model.SelectedTrigger = (sender as GridView).SelectedItem as Trigger;
            }
        }

        private void Motor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as GridView).SelectedItem != null)
            {
                _model.SelectedMotor = (sender as GridView).SelectedItem as Motor;
            }
        }

        private void cbPadNumbers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_model.SelectedUserController.ControllerType == EControllerType.XboxPad)
            {
                uint index = uint.Parse((sender as ComboBox).SelectedItem.ToString());
                _model.XboxPadController = new XboxPadController(index - 1, 100);
                _model.XboxPadController.Connected += XboxPadController_Connected;
                _model.XboxPadController.Disconnected += XboxPadController_Disconnected;
                _model.XboxPadController.StateChanged += XboxPadController_StateChanged;
                tbPadConnected.Visibility = Windows.UI.Xaml.Visibility.Visible;        
                tbPadConnected.Text = "Disconnected";

            }
            else
                tbPadConnected.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

        }

        private void XboxPadController_StateChanged(object sender, System.EventArgs e)
        {
            _model.CheckXboxKeys();
        }

        private void XboxPadController_Disconnected(object sender, System.EventArgs e)
        {
            tbPadConnected.Text = "Disconnected";
        }

        private void XboxPadController_Connected(object sender, System.EventArgs e)
        {
            tbPadConnected.Text = "Connected";
        }
    }
}
