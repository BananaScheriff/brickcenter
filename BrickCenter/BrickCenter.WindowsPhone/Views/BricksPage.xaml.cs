﻿using BrickCenter.Controls;
using BrickCenter.Models;
using BrickCenter.Models.Common;
using BrickCenter.Models.Enumerates;
using BrickCenter.ViewModels;
using Lego.Ev3.Core;
using Lego.Ev3.WinRT;
using System.Collections.ObjectModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using System;
using Windows.UI.Popups;
using BrickCenter.Views;
using BrickCenter.Common;
using Windows.UI.Xaml.Navigation;
using PropertyChanged;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Core;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace BrickCenter.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    /// 
    [ImplementPropertyChanged]
    public sealed partial class BricksPage : Page
    {
        private BricksPageViewModel _model = new BricksPageViewModel();
        public NavigationHelper navigationHelper { get; set; }
        public BricksPage()
        {
            this.InitializeComponent();
            navigationHelper = new NavigationHelper(this);
        }
        #region LEGOconnection
        private ICommunication CreateConnection(ConnectionControl ConnControl, ChooseBrickEventArgs e)
        {
            ICommunication returnType = null;

            switch (e.ConnectionType)
            {
                case ConnectionType.Bluetooth:
                    returnType = new BluetoothCommunication(e.Name, e.Id);
                    break;
                case ConnectionType.Usb:
                    returnType = new UsbCommunication(e.Name, e.Id);
                    break;
                case ConnectionType.WiFi:
                    returnType = new NetworkCommunication(ConnControl.IPAdress);
                    break;
            }
            return returnType;
        }
        private async void ConnectToBrick(object sender, RoutedEventArgs e)
        {
            ConnectionControl control = (ConnectionControl)sender;
            var arg = (e as ChooseBrickEventArgs);
            if (control.BrickController != null && control.BrickController.isConneted)
            {
                Frame.Navigate(typeof(BrickPage), control.BrickController);
            }
            else
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.High, async () =>
                {
                    try
                    {
                        ICommunication connType = CreateConnection(sender as ConnectionControl, arg);
                        Brick brick = new Brick(connType, true);
                        await brick.ConnectAsync();
                        control.BrickController = new BrickController(brick, arg.Name, arg.Id, true);
                        Frame.Navigate(typeof(BrickPage), control.BrickController);
                    }
                    catch (Exception ex)
                    {
                        await new MessageDialog(ex.Message, "Error").ShowAsync();
                    }
                });
            }

        }

        #endregion
        private void GridView_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (e.NewSize.Height != 0)
            {
                BricksPageViewModel.BrickTemplateHeight.Value = e.NewSize.Height / 2.15;
            }
        }
        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
            await _model.Load();
            _model.Frame = Frame;
            DataContext = _model;

        }
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        private void TemplateDragStart(object sender, DragItemsStartingEventArgs e)
        {
            _model.TemplateDragStart(e);
        }

        private void TemplateDrop(object sender, DragEventArgs e)
        {
            _model.TemplateDrop(e);
        }
        private void ListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            var contentPresenter = new Popup();
            contentPresenter.IsOpen = true;
            //Frame.Navigate(typeof(TemplatePage), e.ClickedItem as Template);
        }
    }
}
