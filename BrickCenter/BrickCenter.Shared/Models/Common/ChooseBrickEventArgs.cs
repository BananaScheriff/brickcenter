﻿using BrickCenter.Models.Enumerates;
using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml;

namespace BrickCenter.Models.Common
{
    public class ChooseBrickEventArgs : RoutedEventArgs
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public bool Saved { get; set; } = false;
        public ConnectionType ConnectionType { get; set; }
        public ChooseBrickEventArgs(string name, string id, bool saved, ConnectionType connectionType) : base()
        {
            Name = name;
            Id = id;
            Saved = saved;
            ConnectionType = connectionType;
        }
        public ChooseBrickEventArgs(string name, string id, bool saved) : base()
        {
            Name = name;
            Id = id;
            Saved = saved;
        }

        public ChooseBrickEventArgs()
        {
        }
    }
}
