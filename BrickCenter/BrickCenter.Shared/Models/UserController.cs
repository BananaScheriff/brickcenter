﻿using BrickCenter.Common;
using BrickCenter.Models.Enumerates;
using Lego.Ev3.Core;
using PropertyChanged;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Windows.UI.Core;

namespace BrickCenter.Models
{
    [ImplementPropertyChanged]
    public class UserController
    {
        public EControllerType ControllerType { get; set; } = EControllerType.Keyboard;

        public static ObservableCollection<ObservableCollection<Trigger>> AvailableTriggersDictionary { get; set; } = new ObservableCollection<ObservableCollection<Trigger>>();
        [DependsOn("AvailableTriggersDictionary")]
        public ObservableCollection<Trigger> AvailableTriggers
        {
            get
            {
                return AvailableTriggersDictionary[(int)ControllerType];
            }
        }
        public ObservableCollection<Trigger> Triggers { get; set; } = new ObservableCollection<Trigger>();


        static UserController()
        {
            //TODO wczytaj liste dostepnych triggerow z bazy
            ObservableCollection<Trigger> keyboardTriggers = new ObservableCollection<Trigger>();
            for (char i = 'A'; i <= 'Z'; i++)
                keyboardTriggers.Add(new Trigger(i.ToString(), keyboardTriggers.Count));
            ObservableCollection<Trigger> xboxPadTriggers = new ObservableCollection<Trigger>();
            foreach (var item in new[]
            {
            "A",
            "B",
            "X",
            "Y",
            "Back",
            "Start",
            "DPadDown",
            "DPadLeft",
            "DPadRight",
            "DPadUp",
            "LeftShoulder",
            "LeftThumb",
            "RightShoulder",
            "RightThumb",
            "RightThumbX",
            "RightThumbY",
            "RightTrigger",
            "LeftThumbX",
            "LeftThumbY",
            "LeftTrigger"})
            {
                xboxPadTriggers.Add(new Trigger(item, xboxPadTriggers.Count));
            }


            //0 element
            AvailableTriggersDictionary.Add(keyboardTriggers);
            //1 element
            AvailableTriggersDictionary.Add(xboxPadTriggers);
            //2 element TODO
            AvailableTriggersDictionary.Add(xboxPadTriggers);
        }

        public UserController(EControllerType type)
        {
            ControllerType = type;
        }

        public static void DeleteAvailableTrigger(Trigger trigger, EControllerType type)
        {
            AvailableTriggersDictionary[(int)type].Remove(trigger);
        }
        public static void AddAvailableTrigger(Trigger trigger, EControllerType type)
        {
            if (AvailableTriggersDictionary[(int)type].Count < trigger.Position)
            {
                AvailableTriggersDictionary[(int)type].Insert(AvailableTriggersDictionary[(int)type].Count, trigger);
            }
            else
            {
                AvailableTriggersDictionary[(int)type].Insert(trigger.Position, trigger);
            }
        }
        public UserController()
        {
        }
        public async Task StartTriggerMotorsAtPowerAsync(KeyEventArgs arg, Brick brick)
        {
            foreach (var trigger in Triggers)
            {
                if (arg.VirtualKey.ToString() == trigger.Name)
                {
                    await trigger.StartMotors(brick);
                }
            }
        }

        internal async Task StopTriggerMotorsAsync(KeyEventArgs arg, Brick brick)
        {
            foreach (var trigger in Triggers)
            {
                if (arg.VirtualKey.ToString() == trigger.Name)
                {
                    await trigger.StopMotors(brick);
                }
            }
        }
    }
}
