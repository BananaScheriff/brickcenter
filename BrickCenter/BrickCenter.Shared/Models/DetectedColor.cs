﻿namespace BrickCenter.Models
{
    public enum DetectedColor
    {
        NoColor,
        White,
        Red,
        Blue,
        Green,
        Black

    }
}