﻿using BrickCenter.Models.Enumerates;
using Lego.Ev3.Core;
using PropertyChanged;

namespace BrickCenter.Models
{
    [ImplementPropertyChanged]
    public class BrickController
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public Brick Brick { get; set; }
        public bool isConneted { get; set; }
        public Template Template { get; set; }
        public EControllerType Type { get; set; }
        public int TemplateId { get; internal set; } = -1;

        // public Controller XboxPad { get; set; }

        public BrickController(Brick brick, string name, string id, bool connected)
        {
            Brick = brick;
            Name = name;
            Id = id;          
            Type = EControllerType.XboxPad;
            isConneted = connected;
        }

        public BrickController()
        {
          
        }

        public BrickController(Template template)
        {
            this.Template = template;
            Name = template.Name;
            TemplateId = template.Id;
        }
    }
}
