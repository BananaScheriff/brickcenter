﻿using Lego.Ev3.Core;
using PropertyChanged;

namespace BrickCenter.Models
{
    [ImplementPropertyChanged]
    public class Sensor
    {
        public InputPort BrickInputPort { get; set; }
        public DeviceType SensorType = DeviceType.Empty;
        public double Value { get; set; }
        public double Percentage { get; set; }
        public DetectedColor Color { get; set; }
        public Sensor(InputPort port)
        {
            BrickInputPort = port;
        }
        public Sensor()
        {
        }
        internal void Update(Brick brick)
        {
            SensorType = brick.Ports[BrickInputPort].Type;
            if(SensorType==DeviceType.Color || SensorType == DeviceType.NxtColor)
            {
                brick.Ports[BrickInputPort].SetMode(ColorMode.Color);
                var ColorValue = brick.Ports[BrickInputPort].RawValue;
                switch (ColorValue)
                {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        break;
                    case 7:
                        break;
                }
                //brick.Ports[BrickInputPort].
            }
            Value = brick.Ports[BrickInputPort].RawValue / 10;
            Percentage = brick.Ports[BrickInputPort].PercentValue / 10;
        }
    }
}
