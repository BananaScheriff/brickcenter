﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BrickCenter.Models
{
    public enum UserControllerType
    {
        Keyboard,
        XboxPad,
        PSPad
    }
}
