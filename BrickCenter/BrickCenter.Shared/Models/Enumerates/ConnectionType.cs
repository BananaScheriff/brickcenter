﻿using System;

namespace BrickCenter.Models.Enumerates
{
    public enum ConnectionType
    {
        Unknown = -1,

        Bluetooth,
        WiFi,
        Usb,
    }
    

}
