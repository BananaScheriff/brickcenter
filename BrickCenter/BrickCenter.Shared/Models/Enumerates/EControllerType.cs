﻿
namespace BrickCenter.Models.Enumerates
{
    public enum EControllerType
    {
        Unknown = -1,
        Keyboard = 0,
        XboxPad = 1,
        PSPad = 2
             
    }
   
}
