﻿using System;
using System.Collections.Generic;
using System.Text;
#if WINDOWS_APP
using GameController;
#endif
using PropertyChanged;
using BrickCenter.Common;
using System.Threading;
using Windows.UI.Xaml;

namespace BrickCenter.Models
{
#if WINDOWS_APP
    [ImplementPropertyChanged]
    public class XboxPadController
    {
        public uint PacketNumber { get; set; }
        public uint ControllerId { get; set; }
        public bool IsConnected { get; set; }

        public event EventHandler Disconnected;
        public event EventHandler Connected;
        public event EventHandler StateChanged;

        private Controller CppPadController;
        public ObservableDictionary<string,object> Triggers { get; set; } = new ObservableDictionary<string, object>();
        private DispatcherTimer _timer;

        bool _prevConnState = false;
        bool _currentConnState = false;

        public XboxPadController(uint index,int interval)
        {
        

            CppPadController = new Controller(index);


            Triggers.Add("A", false);
            Triggers.Add("B", false);
            Triggers.Add("X", false);
            Triggers.Add("Y", false);
            Triggers.Add("Back", false);
            Triggers.Add("Start", false);
            Triggers.Add("DPadDown", false);
            Triggers.Add("DPadLeft", false);
            Triggers.Add("DPadRight", false);
            Triggers.Add("DPadUp", false);
            Triggers.Add("LeftShoulder", false);
            Triggers.Add("LeftThumb", false);
            Triggers.Add("RightShoulder", false);
            Triggers.Add("RightThumb", false);

            Triggers.Add("RightThumbX", 0);
            Triggers.Add("RightThumbY", 0);
            Triggers.Add("RightTrigger", 0);

            Triggers.Add("LeftThumbX", 0);
            Triggers.Add("LeftThumbY", 0);
            Triggers.Add("LeftTrigger", 0);

            _timer = new DispatcherTimer();
            _timer.Interval = TimeSpan.FromMilliseconds(interval);
            _timer.Tick += TimerTick;
            _timer.Start();
        }

        private void TimerTick(object sender, object e)
        {
            GetState();
        }

        public void GetState()
        {
            State _state = CppPadController.GetState();

            Triggers["A"] = _state.a;
            Triggers["B"] = _state.b;
            Triggers["X"] = _state.x;
            Triggers["Y"] = _state.y;
            Triggers["Back"] = _state.back;
            Triggers["Start"] = _state.start;
            Triggers["DPadDown"] = _state.dpad_down;
            Triggers["DPadLeft"] = _state.dpad_left;
            Triggers["DPadRight"] = _state.dpad_right;
            Triggers["DPadUp"] = _state.dpad_up;
            Triggers["LeftShoulder"] = _state.left_shoulder;
            Triggers["LeftThumb"] = _state.left_thumb;
            Triggers["RightShoulder"] = _state.right_shoulder;
            Triggers["RightThumb"] = _state.right_thumb;

            Triggers["RightThumbX"] = _state.RightThumbX;
            Triggers["RightThumbY"] = _state.RightThumbY;
            Triggers["RightTrigger"] = _state.RightTrigger;

            Triggers["LeftThumbX"] = _state.LeftThumbX;
            Triggers["LeftThumbY"] = _state.LeftThumbY;
            Triggers["LeftTrigger"] = _state.LeftTrigger;

            if(_prevConnState && !_currentConnState)
            {
                Disconnected?.Invoke(this, null);
            }
            else if(!_prevConnState && _currentConnState)
            {
                Connected?.Invoke(this, null);
            }
            StateChanged?.Invoke(null, null);
            IsConnected = _state.connected;
            _prevConnState = _currentConnState;
            _currentConnState = IsConnected;
        }
    }
#endif
}
