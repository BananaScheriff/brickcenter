﻿using SQLite;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace BrickCenter.Models.ModelDB
{
    [Table("TriggerDB")]
    class TriggerDB
    {
        [PrimaryKey]
        [AutoIncrement]
        public int Id { get; set; }
        [ForeignKey(typeof(UserControllerDB))]
        public int UserControllerId { get; set; }
        public string Name { get; set; }
    }
}
