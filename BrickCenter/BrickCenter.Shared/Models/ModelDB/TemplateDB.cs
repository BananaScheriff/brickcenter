﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace BrickCenter.Models.ModelDB
{
    [Table("TemplateDB")]
    public class TemplateDB
    {
        [PrimaryKey]
        [AutoIncrement]
        public int Id { get; set; }        
        public string Name { get; set; }
    }
}
