﻿using SQLite;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace BrickCenter.Models.ModelDB
{
    [Table("UserControllerDB")]
    class UserControllerDB
    {
        [PrimaryKey]
        [AutoIncrement]
        public int Id { get; set; }
        [ForeignKey(typeof(TemplateDB))]
        public int TemplateId { get; set; }
        public string ControllerType { get; set; }
    }
}

