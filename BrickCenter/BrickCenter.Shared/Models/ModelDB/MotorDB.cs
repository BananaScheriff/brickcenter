﻿using SQLite;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace BrickCenter.Models.ModelDB
{
    [Table("MotorDB")]
    class MotorDB
    {
      

        [PrimaryKey]
        [AutoIncrement]
        public int Id { get; set; }
        [ForeignKey(typeof(TriggerDB))]
        public int TriggerId { get; set; }
        public string BrickInputPort { get; set; }
        public string BrickOutputPort { get; set; }
        public string MotorType { get; set; }
        public int PowerRating { get; set; }
        public bool Reversed { get; set; }
        public bool Analog { get; set; }
    }
}
