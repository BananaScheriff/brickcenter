﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace BrickCenter.Models
{
    [ImplementPropertyChanged]
    public class Template
    {
        public string Name { get; set; }
        public ObservableCollection<UserController> UserControllers { get; set; } = new ObservableCollection<UserController>();
        public int Id { get; set; }
    }
}
