﻿using Lego.Ev3.Core;
using PropertyChanged;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace BrickCenter.Models
{

    [ImplementPropertyChanged]
    public class Trigger
    {
        public ObservableCollection<Motor> Motors { get; set; } = new ObservableCollection<Motor>();
        public string Name { get; set; } = "";
        public int Position { get; set; }
        public Trigger()
        {

        }
        public Trigger(string name,int position)
        {
            Name = name;
            Position = position;
        }
        public async Task StartMotors(Brick brick)
        {
            foreach (var motor in Motors)
            {
                await brick.DirectCommand.TurnMotorAtPowerAsync(motor.BrickOutputPort, motor.PowerRating);
            }
        }
        public async Task StopMotors(Brick brick)
        {
            foreach (var motor in Motors)
            {
                await brick.DirectCommand.StopMotorAsync(motor.BrickOutputPort,true);
            }
        }
    }
}
