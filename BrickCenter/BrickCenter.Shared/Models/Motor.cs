﻿using Lego.Ev3.Core;
using PropertyChanged;

namespace BrickCenter.Models
{
    [ImplementPropertyChanged]
    public class Motor
    {

        public InputPort BrickInputPort { get; set; }
        public OutputPort BrickOutputPort { get; set; }
        public DeviceType MotorType { get; set; } = DeviceType.Empty;
        public bool isPowerShown { get; set; }
        public int PowerRating { get; set; } = 100;
        public bool Reversed { get; set; }
        public bool Analog { get; set; }
        public Motor(Motor tmp)
        {
            BrickInputPort = tmp.BrickInputPort;
            BrickOutputPort = tmp.BrickOutputPort;
            PowerRating = tmp.PowerRating;
            MotorType = tmp.MotorType;
        }
        public Motor(InputPort inputPort, OutputPort outputPort,bool _isPowerShown)
        {
            BrickInputPort = inputPort;
            BrickOutputPort = outputPort;
            isPowerShown = _isPowerShown;
        }
        public Motor()
        {
            BrickInputPort = InputPort.A;
        }

        public void Update(Brick brick)
        {
            MotorType = brick.Ports[BrickInputPort].Type;
        }

      
    }
}
