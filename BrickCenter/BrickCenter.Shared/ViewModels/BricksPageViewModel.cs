﻿using BrickCenter.Common;
using BrickCenter.Models;
using BrickCenter.Models.Common;
using BrickCenter.Models.ModelDB;
using BrickCenter.Views;
using Lego.Ev3.Core;
using PropertyChanged;
using SQLite;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;

namespace BrickCenter.ViewModels
{
    [ImplementPropertyChanged]
    public class BricksPageViewModel
    {
        public static ObservableCollection<BrickController> Bricks { get; set; } = new ObservableCollection<BrickController>();
        public ObservableCollection<Template> Templates { get; set; } = new ObservableCollection<Template>();
        public static ObservalbeDouble BrickTemplateWidth { get; set; } = new ObservalbeDouble() { Value = 180 };
        public static ObservalbeDouble BrickTemplateHeight { get; set; } = new ObservalbeDouble() { Value = 220 };
        #region Commands
        public ICommand AddBrickCommand { get; set; }
        public ICommand AddTemplateCommand { get; set; }
        public ICommand ShowTemplatePopupCommand { get; set; }
        public ICommand CloseTemplatePopupCommand { get; set; }
        public ICommand DeleteBrickControllerCommand { get; set; }
        public ICommand DeleteTemplateCommand { get; set; }
        public ICommand NavigateToCreditsCommand { get; set; }
        public ICommand NavigateToOptionsCommand { get; set; }
        public ICommand NavigateToTutorialsCommand { get; set; }
        #endregion
        #region Popups
        public bool NewTemplatePopupIsOpen { get; set; } = false;
        #endregion
        #region GUI handles
        public string TemplateName { get; set; }
        public Frame Frame { get; set; }
        public BrickController SelectedBrickController { get; set; }
        public Template SelectedTemplate { get; set; }

        private Template DraggedTemplate = null;
        #endregion
        public BricksPageViewModel()
        {
            DeleteBrickControllerCommand = new RelayCommand(() =>
            {
                if (SelectedBrickController != null)
                {
                    if (SelectedBrickController.isConneted)
                        SelectedBrickController.Brick.Disconnect();

                    Bricks.Remove(SelectedBrickController);
                    SelectedBrickController = null;
                }
            });
            NavigateToCreditsCommand = new RelayCommand(() =>
                {
                    Frame.Navigate(typeof(CreditsPage));
                });
            NavigateToOptionsCommand = new RelayCommand(() =>
             {
                 Frame.Navigate(typeof(CreditsPage));
             });
            NavigateToTutorialsCommand = new RelayCommand(() =>
            {
                Frame.Navigate(typeof(CreditsPage));
            });
            AddBrickCommand = new RelayCommand(() =>
            {
                BrickController brick = new BrickController();
                Bricks.Add(brick);
            });
            DeleteTemplateCommand = new RelayCommand(async () =>
            {
                SQLiteAsyncConnection connection = new SQLiteAsyncConnection(APIHelpers.DATABASE_NAME);
                TemplateDB _template = await connection.Table<TemplateDB>().Where(x => x.Id == SelectedTemplate.Id).FirstOrDefaultAsync();
                //Delete old template from db
                var _userControllers = await connection.Table<UserControllerDB>().Where(x => x.TemplateId == _template.Id).ToListAsync();
                foreach (var _controller in _userControllers)
                {
                    var _triggers = await connection.Table<TriggerDB>().Where(x => x.UserControllerId == _controller.Id).ToListAsync();
                    foreach (var _trigger in _triggers)
                    {
                        var _motors = await connection.Table<MotorDB>().Where(x => x.TriggerId == _trigger.Id).ToListAsync();
                        foreach (var _motor in _motors)
                        {
                            await connection.DeleteAsync(_motor);
                        }
                        await connection.DeleteAsync(_trigger);
                    }
                    await connection.DeleteAsync(_controller);
                }
                Templates.Remove(SelectedTemplate);
                await connection.DeleteAsync(_template);
            });
            AddTemplateCommand = new RelayCommand(async () =>
           {
               SQLiteAsyncConnection connection = new SQLiteAsyncConnection(APIHelpers.DATABASE_NAME);
               TemplateDB templateDB = new TemplateDB();
               templateDB.Name = TemplateName;
               await connection.CreateTableAsync<TemplateDB>();
               await connection.InsertAsync(templateDB);
               Template template = new Template() { Name = templateDB.Name };
               Templates.Add(template);
               NewTemplatePopupIsOpen = false;
               TemplateName = "";
           });
            ShowTemplatePopupCommand = new RelayCommand(() =>
            {
                NewTemplatePopupIsOpen = true;
            });
            CloseTemplatePopupCommand = new RelayCommand(() =>
            {
                NewTemplatePopupIsOpen = false;
            });

        }
        public async Task Load()
        {
            SQLiteAsyncConnection connection = new SQLiteAsyncConnection(APIHelpers.DATABASE_NAME);

            if (APIHelpers.DROP)
            {
                await connection.DropTableAsync<TemplateDB>();
                await connection.DropTableAsync<UserControllerDB>();
                await connection.DropTableAsync<TriggerDB>();
                await connection.DropTableAsync<MotorDB>();
            }

            await connection.CreateTableAsync<TemplateDB>();
            await connection.CreateTableAsync<UserControllerDB>();
            await connection.CreateTableAsync<TriggerDB>();
            await connection.CreateTableAsync<MotorDB>();



            var templates = await connection.Table<TemplateDB>().ToListAsync();
            foreach (var template in templates)
            {
                Template _template = new Template();
                _template.Name = template.Name;
                _template.Id = template.Id;
                Templates.Add(_template);
            }
            await Task.Delay(100);
        }
        internal void TemplateDrop(DragEventArgs e)
        {
            Bricks.Add(new BrickController(DraggedTemplate));
            DraggedTemplate = null;
        }
        internal void TemplateDragStart(DragItemsStartingEventArgs e)
        {
            DraggedTemplate = e.Items[0] as Template;
        }
    }


}
