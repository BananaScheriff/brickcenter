﻿using BrickCenter.Models;
using Lego.Ev3.Core;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using System.Windows.Input;
using BrickCenter.Common;
using PropertyChanged;
using System.Linq;
using Windows.UI.Core;
using BrickCenter.Views;
using SQLite;
using BrickCenter.Models.ModelDB;
using BrickCenter.Models.Common;
using System.Threading.Tasks;
using BrickCenter.Models.Enumerates;

namespace BrickCenter.ViewModels
{
    [ImplementPropertyChanged]
    public class BrickPageViewModel
    {
        #region Properties
        public BrickController BrickController { get; set; }
        public ObservableCollection<UserController> UserControllers { get; set; } = new ObservableCollection<UserController>();
        public ObservableCollection<Sensor> Sensors { get; set; } = new ObservableCollection<Sensor>();
        public ObservableCollection<Motor> Motors { get; set; } = new ObservableCollection<Motor>();
        public ObservableCollection<int> PadIndexes { get; set; } = new ObservableCollection<int>() { 1, 2, 3, 4 };
#if WINDOWS_APP
        public XboxPadController XboxPadController { get; set; }
#endif
        public string TemplateName { get; set; }
        #endregion
        #region Commands
        public ICommand ShowTriggerCommand { get; set; }
        public ICommand AddTriggerCommand { get; set; }
        public ICommand CloseTriggerCommand { get; set; }
        public ICommand ShowTemplateCommand { get; set; }
        public ICommand AddTemplateCommand { get; set; }
        public ICommand CloseTemplateCommand { get; set; }

        public ICommand DisconnectBrickCommand { get; set; }
        public ICommand DeleteSelectedCommand { get; set; }
        public ICommand NavigateBackCommand { get; set; }
        #endregion
        #region Popups
        public bool NewTriggerPopupIsOpen { get; set; } = false;
        public bool SaveTemplatePopupIsOpen { get; set; } = false;
        #endregion
        #region GUI handles
        public UserController SelectedUserController { get; set; }
        public int SelectedPad { get; set; } = 0;
        public Trigger SelectedTrigger { get; set; }
        public Trigger SelectedNewTrigger { get; set; }
        public Motor SelectedMotor { get; set; }
        private Motor DraggedMotor;
        public Frame Frame { get; set; }
        #endregion
        public async Task Load()
        {
            SQLiteAsyncConnection connection = new SQLiteAsyncConnection(APIHelpers.DATABASE_NAME, false);

            //Adding a trigger
            ShowTriggerCommand = new RelayCommand(() =>
            {
                if (SelectedUserController != null)
                    NewTriggerPopupIsOpen = true;
            });
            CloseTriggerCommand = new RelayCommand(() =>
            {
                NewTriggerPopupIsOpen = false;
            });
            AddTriggerCommand = new RelayCommand(() =>
            {
                if (SelectedUserController != null && SelectedNewTrigger != null)
                {
                    SelectedUserController.Triggers.Add(SelectedNewTrigger);
                    UserController.DeleteAvailableTrigger(SelectedNewTrigger, SelectedUserController.ControllerType);
                    NewTriggerPopupIsOpen = false;
                }
            });

            //Saving current configuration as a template
            ShowTemplateCommand = new RelayCommand(() =>
            {
                SaveTemplatePopupIsOpen = true;
            });
            CloseTemplateCommand = new RelayCommand(() =>
            {
                TemplateName = "";
                SaveTemplatePopupIsOpen = false;
            });
            AddTemplateCommand = new RelayCommand(async () =>
            {
                var templates = await connection.Table<TemplateDB>().ToListAsync();
                if (templates.Any(x => x.Name == TemplateName))
                {

                }
                else
                {
                    await connection.InsertAsync(new TemplateDB() { Name = TemplateName });
                    TemplateDB _template = await connection.Table<TemplateDB>().Where(x => x.Name == TemplateName).FirstOrDefaultAsync();

                    foreach (var userController in UserControllers)
                    {
                        await connection.InsertAsync(new UserControllerDB() {
                            TemplateId = _template.Id,
                            ControllerType = userController.ControllerType.ToString() });
                        string conntrollerType = userController.ControllerType.ToString();
                        UserControllerDB _userController = await connection.Table<UserControllerDB>().Where
                        (x => x.TemplateId == _template.Id && 
                        x.ControllerType == conntrollerType).FirstOrDefaultAsync();
                        foreach (var trigger in userController.Triggers)
                        {
                            await connection.InsertAsync(new TriggerDB() {
                                UserControllerId = _userController.Id,
                                Name = trigger.Name
                            });
                            TriggerDB _trigger = await connection.Table<TriggerDB>().Where(
                                x => x.UserControllerId == _userController.Id &&
                                x.Name == trigger.Name).FirstOrDefaultAsync();

                            foreach (var motor in trigger.Motors)
                            {
                                MotorDB _motor = new MotorDB();
                                _motor.TriggerId = _trigger.Id;
                                _motor.BrickInputPort = motor.BrickInputPort.ToString();
                                _motor.BrickOutputPort = motor.BrickOutputPort.ToString();
                                _motor.MotorType = motor.MotorType.ToString();
                                _motor.PowerRating = motor.PowerRating;
                                _motor.Reversed = motor.Reversed;
                                _motor.Analog = motor.Analog;
                                await connection.InsertAsync(_motor);
                            }
                        }
                    }
                    TemplateName = "";
                    SaveTemplatePopupIsOpen = false;
                }
            });


            DisconnectBrickCommand = new RelayCommand(() =>
            {
                BrickController.Brick._comm.BrickDisconnected += _comm_BrickDisconnected;
                BrickController.Brick.Disconnect();
                
            });
            NavigateBackCommand = new RelayCommand(() =>
            {
                Frame.Navigate(typeof(BricksPage));
            });
            DeleteSelectedCommand = new RelayCommand(() =>
            {
                if (SelectedTrigger != null && SelectedUserController != null)
                {
                    UserController.AddAvailableTrigger(SelectedTrigger, SelectedUserController.ControllerType);
                    SelectedUserController.Triggers.Remove(SelectedTrigger);
                    SelectedTrigger = null;
                }
                if (SelectedMotor != null && SelectedUserController != null)
                {
                    SelectedUserController.Triggers.First(x => x.Motors.Contains(SelectedMotor))?.Motors.Remove(SelectedMotor);
                    SelectedMotor = null;
                }
            });
            //if template was used
            if (BrickController.TemplateId != -1)
            {
                //Load template from database
                var userControllers = await connection.Table<UserControllerDB>().Where(x => x.TemplateId == BrickController.TemplateId).ToListAsync();
                foreach (var userController in userControllers)
                {
                    UserController _userController = new UserController();
                    _userController.ControllerType = userController.ControllerType.ToEnum<EControllerType>();
                    var triggers = await connection.Table<TriggerDB>().Where(x => x.UserControllerId == userController.Id).ToListAsync();
                    foreach (var trigger in triggers)
                    {
                        Trigger _trigger = new Trigger();
                        _trigger.Name = trigger.Name;
                        var motors = await connection.Table<MotorDB>().Where(x => x.TriggerId == trigger.Id).ToListAsync();
                        foreach (var motor in motors)
                        {
                            Motor _motor = new Motor();
                            _motor.PowerRating = motor.PowerRating;
                            _motor.Reversed = motor.Reversed;
                            _motor.Analog = motor.Analog;
                            _motor.BrickInputPort = motor.BrickInputPort.ToEnum<InputPort>();
                            _motor.BrickOutputPort = motor.BrickOutputPort.ToEnum<OutputPort>();
                            _motor.MotorType = motor.MotorType.ToEnum<DeviceType>();
                            _trigger.Motors.Add(_motor);
                        }
                        _userController.Triggers.Add(_trigger);
                    }
                    UserControllers.Add(_userController);
                }
            }
            //if new empty brick was used
            else
            {
                //add empty controllers
                UserControllers.Add(new UserController(EControllerType.Keyboard));
                UserControllers.Add(new UserController(EControllerType.XboxPad));
            }
            // Add sensor ports
            foreach (InputPort port in new List<InputPort> { InputPort.One, InputPort.Two, InputPort.Three, InputPort.Four })
                Sensors.Add(new Sensor(port));
            // Add motor ports
            foreach (var ports in new[]
            {
                new { Input = InputPort.A, Output= OutputPort.A },
                new { Input = InputPort.B, Output = OutputPort.B },
                new { Input = InputPort.C, Output = OutputPort.C },
                new { Input = InputPort.D, Output = OutputPort.D }
            })
            {
                Motors.Add(new Motor(ports.Input, ports.Output, false));
            }
            // Choose first controller
            if (UserControllers.Count > 0)
                SelectedUserController = UserControllers[0];
        }

        private void _comm_BrickDisconnected(object sender, BrickDisconnectedEventArgs e)
        {
            BrickController.isConneted = false;
            Frame.Navigate(typeof(BricksPage));
        }

        internal async void CheckXboxKeys()
        {
            //if(SelectedUserController.ControllerType == UserControllerType.XboxPad && XboxPadController.IsConnected)
            //for (int i = 0; i < SelectedUserController.Triggers.Count; i++)
            //{
            //        for (int ii = 0; ii < SelectedUserController.Triggers[i].Motors.Count; ii++)
            //        {
            //            await SelectedUserController.Triggers[i].StartMotors(BrickController.Brick);
            //        }
            //}
        }

        internal void MotorDragStarting(DragItemsStartingEventArgs e)
        {
            DraggedMotor = new Motor(e.Items[0] as Motor);
            DraggedMotor.isPowerShown = true;
        }
        internal void DropMotorOnTrigger(object sender, DragEventArgs e)
        {
            if ((e.OriginalSource is Grid))
            {
                Trigger trigger = (e.OriginalSource as Grid).DataContext as Trigger;
                if (trigger != null)
                {
                    var controllerTrigger = SelectedUserController.Triggers.FirstOrDefault(x => x.Name == trigger.Name);
                    if (!controllerTrigger.Motors.Contains(DraggedMotor))
                        controllerTrigger.Motors.Add(DraggedMotor);
                }
            }

        }
        internal async void KeyboardKeyReleased(KeyEventArgs arg)
        {
            if (SelectedUserController.ControllerType == EControllerType.Keyboard)
            {
                await SelectedUserController.StopTriggerMotorsAsync(arg, BrickController.Brick);
            }
        }
        internal async void KeyboardKeyPressed(KeyEventArgs arg)
        {
            if (SelectedUserController.ControllerType == EControllerType.Keyboard)
            {
                await SelectedUserController.StartTriggerMotorsAtPowerAsync(arg, BrickController.Brick);
            }

        }
    }
}
