﻿using BrickCenter.Common;
using BrickCenter.Models;
using BrickCenter.Models.Common;
using BrickCenter.Models.Enumerates;
using BrickCenter.Models.ModelDB;
using BrickCenter.Views;
using Lego.Ev3.Core;
using PropertyChanged;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace BrickCenter.ViewModels
{
    [ImplementPropertyChanged]
    public class TemplatePageViewModel
    {
        #region properties
        public ObservableCollection<UserController> UserControllers { get; set; } = new ObservableCollection<UserController>();
        public ObservableCollection<Sensor> Sensors { get; set; } = new ObservableCollection<Sensor>();
        public ObservableCollection<Motor> Motors { get; set; } = new ObservableCollection<Motor>();
        public ObservableCollection<int> PadIndexes { get; set; } = new ObservableCollection<int>() { 1, 2, 3, 4 };
        public string Name { get; set; }
        #endregion
        #region Commands
        public ICommand ShowTriggerCommand { get; set; }
        public ICommand AddTriggerCommand { get; set; }
        public ICommand CloseTriggerCommand { get; set; }
        public ICommand DisconnectBrickCommand { get; set; }
        public ICommand SaveTemplateCommand { get; set; }
        public ICommand DeleteSelectedCommand { get; set; }
        public ICommand NavigateBackCommand { get; set; }

        #endregion
        #region Popups
        public bool NewTriggerPopupIsOpen { get; set; } = false;
        public bool SaveTemplatePopupIsOpen { get; set; } = false;
        #endregion
        #region GUI handles
        public Frame Frame { get; set; }
        public UserController SelectedUserController { get; set; }
        public int SelectedPad { get; set; } = 0;
        public Trigger SelectedTrigger { get; set; }
        public Trigger SelectedNewTrigger { get; set; }

        public Motor SelectedMotor { get; set; }
        private Motor DraggedMotor;
        #endregion
        public async Task Load(Template template)
        {
            Name = template.Name;

            SQLiteAsyncConnection connection = new SQLiteAsyncConnection(APIHelpers.DATABASE_NAME, false);

            //Adding a trigger
            ShowTriggerCommand = new RelayCommand(() =>
            {
                if (SelectedUserController != null)
                    NewTriggerPopupIsOpen = true;
            });
            CloseTriggerCommand = new RelayCommand(() =>
            {
                NewTriggerPopupIsOpen = false;
            });
            AddTriggerCommand = new RelayCommand(() =>
            {
                if (SelectedUserController != null && SelectedNewTrigger != null)
                {
                    SelectedUserController.Triggers.Add(SelectedNewTrigger);
                    UserController.DeleteAvailableTrigger(SelectedNewTrigger, SelectedUserController.ControllerType);
                    NewTriggerPopupIsOpen = false;
                }
            });
            SaveTemplateCommand = new RelayCommand(async () =>
            {
                TemplateDB _template = await connection.Table<TemplateDB>().Where(x => x.Name == Name).FirstOrDefaultAsync();
                //Delete old template from db
                var _userControllers = await connection.Table<UserControllerDB>().Where(x => x.TemplateId == _template.Id).ToListAsync();
                foreach (var _controller in _userControllers)
                {
                    var _triggers = await connection.Table<TriggerDB>().Where(x => x.UserControllerId == _controller.Id).ToListAsync();
                    foreach (var _trigger in _triggers)
                    {
                        var _motors = await connection.Table<MotorDB>().Where(x => x.TriggerId == _trigger.Id).ToListAsync();
                        foreach (var _motor in _motors)
                        {
                            await connection.DeleteAsync(_motor);
                        }
                        await connection.DeleteAsync(_trigger);
                    }
                    await connection.DeleteAsync(_controller);
                }
                //Save new template to db
                foreach (var userController in UserControllers)
                {
                    await connection.InsertAsync(new UserControllerDB()
                    {
                        TemplateId = _template.Id,
                        ControllerType = userController.ControllerType.ToString()
                    });
                    string conntrollerType = userController.ControllerType.ToString();
                    UserControllerDB _userController = await connection.Table<UserControllerDB>().Where
                    (x => x.TemplateId == _template.Id &&
                    x.ControllerType == conntrollerType).FirstOrDefaultAsync();
                    foreach (var trigger in userController.Triggers)
                    {
                        await connection.InsertAsync(new TriggerDB()
                        {
                            UserControllerId = _userController.Id,
                            Name = trigger.Name
                        });
                        TriggerDB _trigger = await connection.Table<TriggerDB>().Where(
                            x => x.UserControllerId == _userController.Id &&
                            x.Name == trigger.Name).FirstOrDefaultAsync();

                        foreach (var motor in trigger.Motors)
                        {
                            MotorDB _motor = new MotorDB();
                            _motor.TriggerId = _trigger.Id;
                            _motor.BrickInputPort = motor.BrickInputPort.ToString();
                            _motor.BrickOutputPort = motor.BrickOutputPort.ToString();
                            _motor.MotorType = motor.MotorType.ToString();
                            _motor.PowerRating = motor.PowerRating;
                            _motor.Reversed = motor.Reversed;
                            _motor.Analog = motor.Analog;
                            await connection.InsertAsync(_motor);
                        }
                    }
                }
            });
            DeleteSelectedCommand = new RelayCommand(() =>
            {
                //Remove trigger
                if (SelectedTrigger != null && SelectedUserController != null)
                {
                    UserController.AddAvailableTrigger(SelectedTrigger, SelectedUserController.ControllerType);
                    SelectedUserController.Triggers.Remove(SelectedTrigger);
                    SelectedTrigger = null;
                }
                if (SelectedMotor != null && SelectedUserController != null)
                {
                    SelectedUserController.Triggers.First(x => x.Motors.Contains(SelectedMotor))?.Motors.Remove(SelectedMotor);
                    SelectedMotor = null;
                }
            });
            NavigateBackCommand = new RelayCommand(() =>
            {
                Frame.Navigate(typeof(BricksPage));
            });


            var userControllers = await connection.Table<UserControllerDB>().Where(x => x.TemplateId == template.Id).ToListAsync();
            if (userControllers.Count > 0)
            {
                foreach (var userController in userControllers)
                {
                    UserController _userController = new UserController();
                    _userController.ControllerType = userController.ControllerType.ToEnum<EControllerType>();
                    var triggers = await connection.Table<TriggerDB>().Where(x => x.UserControllerId == userController.Id).ToListAsync();
                    foreach (var trigger in triggers)
                    {
                        Trigger _trigger = new Trigger();
                        _trigger.Name = trigger.Name;
                        var motors = await connection.Table<MotorDB>().Where(x => x.TriggerId == trigger.Id).ToListAsync();
                        foreach (var motor in motors)
                        {
                            Motor _motor = new Motor();
                            _motor.PowerRating = motor.PowerRating;
                            _motor.Reversed = motor.Reversed;
                            _motor.Analog = motor.Analog;
                            _motor.BrickInputPort = motor.BrickInputPort.ToEnum<InputPort>();
                            _motor.BrickOutputPort = motor.BrickOutputPort.ToEnum<OutputPort>();
                            _motor.MotorType = motor.MotorType.ToEnum<DeviceType>();
                            _trigger.Motors.Add(_motor);
                        }
                        _userController.Triggers.Add(_trigger);
                    }
                    UserControllers.Add(_userController);
                }
            }
            //if new empty brick was used
            else
            {
                //add empty controllers
                UserControllers.Add(new UserController(EControllerType.Keyboard));
                UserControllers.Add(new UserController(EControllerType.XboxPad));
            }
            // Add sensor ports
            foreach (InputPort port in new List<InputPort> { InputPort.One, InputPort.Two, InputPort.Three, InputPort.Four })
                Sensors.Add(new Sensor(port));
            // Add motor ports
            foreach (var ports in new[]
            {
                new { Input = InputPort.A, Output= OutputPort.A },
                new { Input = InputPort.B, Output = OutputPort.B },
                new { Input = InputPort.C, Output = OutputPort.C },
                new { Input = InputPort.D, Output = OutputPort.D }
            })
            {
                Motors.Add(new Motor(ports.Input, ports.Output, false));
            }
            // Choose first controller
            if (UserControllers.Count > 0)
                SelectedUserController = UserControllers[0];
        }

        internal void MotorDragStarting(DragItemsStartingEventArgs e)
        {
            DraggedMotor = e.Items[0] as Motor;
        }
        internal void DropMotorOnTrigger(object sender, DragEventArgs e)
        {
            if ((e.OriginalSource is Grid))
            {
                Trigger trigger = (e.OriginalSource as Grid).DataContext as Trigger;
                if (trigger != null)
                {
                    var controllerTrigger = SelectedUserController.Triggers.FirstOrDefault(x => x.Name == trigger.Name);
                    if (!controllerTrigger.Motors.Contains(DraggedMotor))
                        controllerTrigger.Motors.Add(DraggedMotor);
                }
            }

        }

    }
}
