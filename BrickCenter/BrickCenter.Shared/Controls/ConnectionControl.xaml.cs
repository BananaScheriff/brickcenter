﻿using BrickCenter.Models;
using BrickCenter.ViewModels;
using PropertyChanged;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using Windows.Devices.Bluetooth.Rfcomm;
using Windows.Devices.Enumeration;
using Windows.Devices.HumanInterfaceDevice;
using Windows.UI.Xaml;
using System.Linq;
using Windows.UI.Xaml.Controls;
using System;
using BrickCenter.Models.Enumerates;
using BrickCenter.Models.Common;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace BrickCenter.Controls
{
    [ImplementPropertyChanged]
    public sealed partial class ConnectionControl : UserControl
    {
        public ObservableCollection<string> ConnectionTypes { get; set; } = new ObservableCollection<string>() { "Bluetooth", "USB", "Wi-Fi" };
        public string SelectedType { get; set; } = "Bluetooth";
        public string IPAdress { get; set; } = "192.168.1.10";
        public int BrickIndex { get; set; }
        public string BrickName { get; set; }

        public ObservableCollection<KeyValuePair<string, string>> BrickListBT = new ObservableCollection<KeyValuePair<string, string>>();
        public ObservableCollection<string> BrickNamesBT { get; set; } = new ObservableCollection<string>();
        public ObservableCollection<KeyValuePair<string, string>> BrickListUSB = new ObservableCollection<KeyValuePair<string, string>>();
        public ObservableCollection<string> BrickNamesUSB { get; set; } = new ObservableCollection<string>();

        DeviceInformationCollection Devices { get; set; }
        private const UInt16 VID = 0x0694;
        private const UInt16 PID = 0x0005;
        private const UInt16 UsagePage = 0xff00;
        private const UInt16 UsageId = 0x0001;

        public event RoutedEventHandler ConnectToBrick;
        public event RoutedEventHandler DiconnectBrick;


        public BrickController BrickController
        {
            get { return (BrickController)GetValue(BrickControllerProperty); }
            set { SetValue(BrickControllerProperty, value); }
        }

        // Using a DependencyProperty as the backing store for BrickController.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BrickControllerProperty =
            DependencyProperty.Register("BrickController", typeof(BrickController), typeof(ConnectionControl), new PropertyMetadata(new BrickController(),OnBrickChanged ));

        public static void OnBrickChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }

        public ObservalbeDouble BrickTemplateHeight { get; set; } = BricksPageViewModel.BrickTemplateHeight;
        public ConnectionControl()
        {
            this.InitializeComponent();
            BrickTemplateHeight.PropertyChanged += BrickTemplateHeight_PropertyChanged;
            DataContext = this;
            GetDevisec();
        }

        private async void GetDevisec()
        {
            string selector = RfcommDeviceService.GetDeviceSelector(RfcommServiceId.SerialPort);
            Devices = await DeviceInformation.FindAllAsync(selector);
            foreach (DeviceInformation item in Devices)
            {
                BrickListBT.Add(new KeyValuePair<string, string>(item.Name, item.Id));
                BrickNamesBT.Add(item.Name);
            }
            selector = HidDevice.GetDeviceSelector(UsagePage, UsageId, VID, PID);
            Devices = await DeviceInformation.FindAllAsync(selector);
            foreach (DeviceInformation item in Devices)
            {
                BrickListUSB.Add(new KeyValuePair<string, string>(item.Name, item.Id));
                BrickNamesUSB.Add(item.Name);
            }
        }


        private void BrickTemplateHeight_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (BrickTemplateHeight.Value > 220)
                this.Height = BrickTemplateHeight.Value;

        }
        public ConnectionType GetConnectionType()
        {
            ConnectionType conType;
            Enum.TryParse(SelectedType, true, out conType);
            return conType;
        }
        private void Connect_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            if (ConnectToBrick != null)
            {

                var connType = GetConnectionType();
                if (connType == ConnectionType.Bluetooth)
                {
                    if (BrickIndex != -1)
                    {
                        e = new ChooseBrickEventArgs(BrickListBT[BrickIndex].Key, BrickListBT[BrickIndex].Value, false, connType);
                        ConnectToBrick.Invoke(this, e);
                    }
                }
                else if (connType == ConnectionType.Usb)
                {
                    if (BrickIndex != -1)
                    {
                        e = new ChooseBrickEventArgs(BrickListUSB[BrickIndex].Key, BrickListUSB[BrickIndex].Value, false, connType);
                        ConnectToBrick.Invoke(this, e);
                    }
                }
                else
                {
                    e = new ChooseBrickEventArgs();
                    ConnectToBrick.Invoke(this, e);
                }


            }
        }

        private void Disconnect_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            if (DiconnectBrick != null)
            {
                e = new ChooseBrickEventArgs(BrickController.Name, BrickController.Id, false);
                DiconnectBrick.Invoke(this, e);
            }
        }
    }
}
