﻿using PropertyChanged;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace BrickCenter.Controls
{
    [ImplementPropertyChanged]
    public sealed partial class MotorControl : UserControl
    {
        public MotorControl()
        {
            this.InitializeComponent();
            slider.Maximum = 100;
            slider.Minimum = -100;
        }
    }
}
