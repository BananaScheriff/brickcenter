﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace BrickCenter.Converters
{
    public class IndexToVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var boolValue = false;

            if (parameter != null)
            {
                boolValue = value.ToString() == parameter.ToString();
            }
            else
            {
                boolValue = System.Convert.ToInt32(value) == 0;
            }

            return boolValue ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
