﻿using BrickCenter.Models.Enumerates;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace BrickCenter.Converters
{
    public class ConnectionTypeToVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            string sValue = value.ToString();
            return sValue == (string)parameter ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
